FROM jupyter/minimal-notebook:584f43f06586

RUN pip install --upgrade pip
RUN pip install pandas==1.2.4



# Udacity's Data Science Nanodegree requirements
# ==============================================

RUN pip install matplotlib==3.4.2

