# Preliminary

 1) In your project root folder you must create a `data` subfolder. This folder will be mounted into the container, so files created in Jupyter Notebook will be accessible on your local machine.
 2) Create a custom password to access the Jupyter Notebook. This will need to be hashed and passed to `docker-compose.yml` when it spins up the container. A handy utility to create a hashed password:
    
    ```sh
    $ python3 -c "from IPython.lib.security import passwd; print(passwd(passphrase='your_password', algorithm='sha1'))"
    ```
 3) Create a `.env` environmental file and paste the hashed password. It should look like this:
    
    ```sh
    JUPYTER_PASSWORD_SHA1 = sha1:lkdsandaiufg7asgga883274327462sfdsdf
    ```

# Run container

```sh
$ docker compose up -d
```

`-d` option will run containers in the background, print new container names.

Use `--build` option to rebuild image before starting containers.

# Stop container

```sh
$ docker compose down
```

# Access Jupyter Notebook

Type `localhost:8765` or `127.0.0.1:8765` or `0.0.0.0:8765` in your browser.

> Note: the `8765` port is being used to access the notebook instance instead of the default `8888`.
